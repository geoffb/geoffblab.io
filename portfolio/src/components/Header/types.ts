export type HeaderLink = {
  slug: string
  title: string
}
