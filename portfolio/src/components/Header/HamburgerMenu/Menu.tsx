import React from 'react'
import Link from 'next/link'
import { links } from '../constants'

const Menu: React.FC = () => (
  <div className='drawer'>
    <input id='my-drawer' type='checkbox' className='drawer-toggle' />
    <div className='drawer-side'>
      <label
        htmlFor='my-drawer'
        aria-label='close sidebar'
        className='drawer-overlay'
      ></label>
      <div className='menu p-4 w-80 min-h-full bg-stone-200 dark:bg-zinc-900'>
        {links.map(({ slug, title }) => (
          <Link
            className='py-4 text-zinc-900 dark:text-white hover:underline'
            href={slug}
            key={title}
          >
            {title}
          </Link>
        ))}
      </div>
    </div>
  </div>
)

export default Menu
