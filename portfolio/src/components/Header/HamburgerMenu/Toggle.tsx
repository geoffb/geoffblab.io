import React from 'react'
import { Menu } from 'lucide-react'

const Toggle: React.FC = () => (
  <label htmlFor='my-drawer' className='drawer-button md:hidden'>
    <Menu
      aria-controls='drawer-example'
      className='w-6 h-6 text-zinc-900 dark:text-white'
      data-drawer-show='drawer-example'
      data-drawer-target='drawer-example'
      role='button'
      type='button'
    />
  </label>
)

export default Toggle
