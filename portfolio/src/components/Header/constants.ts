import type { HeaderLink } from './types'

export const links: HeaderLink[] = [
  { slug: '/projects', title: 'Projects' },
  { slug: '/articles', title: 'Articles' },
  { slug: '/contact', title: 'Contact' },
  { slug: '/about', title: 'About' },
]
