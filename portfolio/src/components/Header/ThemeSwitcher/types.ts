export const ThemeValues = {
  dark: 'dark',
  light: 'light',
  system: 'system',
} as const
