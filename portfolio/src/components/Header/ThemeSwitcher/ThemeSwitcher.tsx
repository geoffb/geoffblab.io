import React, { useCallback, useMemo } from 'react'
import { useTheme } from 'next-themes'
import { MoonStar, Sun } from 'lucide-react'
import { ThemeValues } from './types'

const ThemeSwitcher: React.FC = () => {
  const { setTheme, systemTheme, theme } = useTheme()
  const currentTheme = useMemo(
    () => (theme === ThemeValues.system ? systemTheme : theme),
    [systemTheme, theme]
  )
  const onClick = useCallback(
    ({ themeValue }: { themeValue: keyof typeof ThemeValues }) =>
      setTheme(themeValue),
    [setTheme]
  )

  if (currentTheme === ThemeValues.dark) {
    return (
      <Sun
        className='w-6 h-6 text-zinc-900 dark:text-white'
        onClick={() => onClick({ themeValue: ThemeValues.light })}
        role='button'
      />
    )
  }

  return (
    <MoonStar
      className='w-6 h-6 text-zinc-900 dark:text-white'
      onClick={() => onClick({ themeValue: ThemeValues.dark })}
      role='button'
    />
  )
}

export default ThemeSwitcher
