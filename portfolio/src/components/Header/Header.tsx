import dynamic from 'next/dynamic'
import Link from 'next/link'
import { Toggle } from './HamburgerMenu'
import { links } from './constants'

const ThemeSwitcher = dynamic(() => import('./ThemeSwitcher'), { ssr: false })

const Header: React.FC = () => (
  <header className='h-14 flex items-center justify-between dark:text-white border-b-[1px] border-gray-500 px-4'>
    <Link href='/'>
      <h1 className='font-heading text-zinc-900 dark:text-white text-3xl'>
        geoffb
      </h1>
    </Link>
    <div className='flex gap-10 max-md:hidden'>
      {links.map(({ slug, title }) => (
        <Link
          className='px-4 text-zinc-900 dark:text-white hover:underline'
          href={slug}
          key={title}
        >
          {title}
        </Link>
      ))}
    </div>
    <div className='flex gap-4'>
      <Toggle />
      <ThemeSwitcher />
    </div>
  </header>
)

export default Header
