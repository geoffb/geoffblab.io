import React from 'react'
import Header from '../Header'
import { Menu } from '../Header/HamburgerMenu'
import type { ReactChildren } from '../types'
import useFeatureFlag from '~/utils/featureFlags/useFeatureFlag'
import { FEATURE_FLAG_KEYS } from '~/utils/featureFlags/types'

const Layout: React.FC<ReactChildren> = ({ children }) => {
  const displayMenu = useFeatureFlag({
    featureFlag: FEATURE_FLAG_KEYS.display_menu,
  })

  return (
    <div className='min-h-screen min-w-full bg-stone-200 dark:bg-zinc-900'>
      {displayMenu && (
        <>
          <Menu />
          <Header />
        </>
      )}
      {children}
    </div>
  )
}

export default Layout
