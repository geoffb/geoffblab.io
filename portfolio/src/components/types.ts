import type React from 'react'

export type ReactChildren = { children: React.ReactNode }
