import { createServerSideHelpers } from '@trpc/react-query/server'
import SuperJSON from 'superjson'
import { type AppRouter, appRouter } from './api/root'
import { createInnerTRPCContext } from './context'

export const ssgInit = () => {
  return createServerSideHelpers<AppRouter>({
    router: appRouter,
    ctx: createInnerTRPCContext(),
    transformer: SuperJSON,
  })
}
