import { db } from './db'

export const createInnerTRPCContext = () => ({
  db,
})
