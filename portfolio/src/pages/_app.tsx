import { type AppType } from 'next/app'
import { ThemeProvider } from 'next-themes'
import Layout from '~/components/Layout'
import { api } from '~/utils/api'
import '~/styles/globals.css'
import { FeatureFlagProvider } from '~/utils/featureFlags'

const Portfolio: AppType = ({ Component, pageProps }) => (
  <ThemeProvider enableSystem attribute='class'>
    <FeatureFlagProvider>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </FeatureFlagProvider>
  </ThemeProvider>
)

export default api.withTRPC(Portfolio)
