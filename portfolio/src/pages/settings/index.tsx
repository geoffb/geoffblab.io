import React, { useContext } from 'react'
import { FeatureFlagContext } from '~/utils/featureFlags'
import type { FeatureFlag } from '~/utils/featureFlags/types'

const Settings: React.FC = () => {
  const { featureFlags, updateFeatureFlag } = useContext(FeatureFlagContext)
  const featureFlagsIterator = Object.fromEntries(featureFlags)

  return (
    <main className='flex flex-col items-center justify-center'>
      <div className='container flex flex-col items-center justify-center gap-6 px-4 py-16 prose'>
        <h1 className='text-teal-600 dark:text-white'>Settings</h1>
        {Object.entries(featureFlagsIterator).map(
          ([featureFlagName, value]) => (
            <div className='form-control' key={featureFlagName}>
              <label className='label cursor-pointer w-80 '>
                <span className='label-text text-zinc-900 dark:text-white capitalize font-bold'>
                  {featureFlagName.replaceAll('_', ' ')}
                </span>
                <input
                  type='checkbox'
                  className='toggle'
                  checked={value}
                  onChange={() =>
                    updateFeatureFlag({
                      featureFlag: featureFlagName as FeatureFlag,
                      value: !value,
                    })
                  }
                />
              </label>
            </div>
          )
        )}
      </div>
    </main>
  )
}

export default Settings
