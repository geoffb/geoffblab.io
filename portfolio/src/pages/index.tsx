import Head from 'next/head'

const Home = () => (
  <>
    <Head>
      <title>Geoffroy Baumier</title>
      <meta name='description' content='Website in construction' />
      <link rel='icon' href='/favicon.ico' />
    </Head>
    <main className='flex flex-col items-center justify-center'>
      <div className='container flex flex-col items-center justify-center gap-12 px-4 py-16 '>
        <h1 className='dark:text-white'>Website in construction..</h1>
      </div>
    </main>
  </>
)

export default Home
