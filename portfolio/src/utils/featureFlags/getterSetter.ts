import type { GetterSetter, Setter } from './types'

export const getFeatureFlagFromLocalStorage = ({
  featureFlag,
}: GetterSetter) => {
  if (typeof window !== 'undefined') {
    return localStorage?.getItem(featureFlag) === 'true' ?? false
  }

  return false
}

export const setFeatureFlagToLocalStorage = ({
  featureFlag,
  value,
}: Setter) => {
  if (typeof window !== 'undefined') {
    return localStorage?.setItem(featureFlag, `${value}`)
  }
}
