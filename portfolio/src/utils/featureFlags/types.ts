export const FEATURE_FLAG_KEYS = {
  display_menu: 'display_menu',
} as const

export type FeatureFlag = keyof typeof FEATURE_FLAG_KEYS

export type GetterSetter = {
  featureFlag: FeatureFlag
}

export type Setter = GetterSetter & {
  value: boolean
}

export type FeatureFlagContext = {
  featureFlags: Map<FeatureFlag, boolean>
  updateFeatureFlag: ({ featureFlag, value }: Setter) => void
}
