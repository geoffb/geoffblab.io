import { useContext } from 'react'
import type { FeatureFlag } from './types'
import { FeatureFlagContext } from './context'

const useFeatureFlag = ({ featureFlag }: { featureFlag: FeatureFlag }) => {
  const { featureFlags } = useContext(FeatureFlagContext)

  return featureFlags.get(featureFlag) ?? false
}

export default useFeatureFlag
