import { useCallback, useEffect, useState } from 'react'
import type { ReactChildren } from '~/components/types'
import { FeatureFlagContext } from './context'
import { FEATURE_FLAG_KEYS, type FeatureFlag, type Setter } from './types'
import {
  getFeatureFlagFromLocalStorage,
  setFeatureFlagToLocalStorage,
} from './getterSetter'

const FeatureFlagProvider: React.FC<ReactChildren> = ({ children }) => {
  const [featureFlags, setFeatureFlags] = useState<Map<FeatureFlag, boolean>>(
    new Map()
  )
  const updateFeatureFlag = useCallback(
    ({ featureFlag, value }: Setter) => {
      const newFeatureFlags = new Map(featureFlags)

      newFeatureFlags.set(featureFlag, value)

      setFeatureFlags(newFeatureFlags)
      setFeatureFlagToLocalStorage({ featureFlag, value })
    },
    [featureFlags]
  )

  useEffect(() => {
    const updatedFeatureFlags = new Map()

    Object.keys(FEATURE_FLAG_KEYS).map((featureFlag) =>
      updatedFeatureFlags.set(
        featureFlag as FeatureFlag,
        getFeatureFlagFromLocalStorage({
          featureFlag: featureFlag as FeatureFlag,
        })
      )
    )

    setFeatureFlags(updatedFeatureFlags)
  }, [])

  return (
    <FeatureFlagContext.Provider value={{ featureFlags, updateFeatureFlag }}>
      {children}
    </FeatureFlagContext.Provider>
  )
}

export { FeatureFlagProvider }
