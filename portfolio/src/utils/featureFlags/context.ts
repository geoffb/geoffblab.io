import { createContext } from 'react'
import { type FeatureFlagContext as FFContext } from './types'

const FeatureFlagContext = createContext<FFContext>({
  featureFlags: new Map(),
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  updateFeatureFlag: () => {},
})

export { FeatureFlagContext }
