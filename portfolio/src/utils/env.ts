const NODE_ENV_VALUES = {
  development: 'development',
  production: 'production',
} as const

export const isLive =
  process.env.NODE_ENV === NODE_ENV_VALUES.production ||
  process.env.NEXT_PUBLIC_NODE_ENV === NODE_ENV_VALUES.production
