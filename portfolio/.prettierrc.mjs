const config = {
  bracketSameLine: false,
  bracketSpacing: true,
  jsxSingleQuote: true,
  semi: false,
  singleQuote: true,
  tabWidth: 2,
  trailingComma: 'es5',
  useTabs: false,
}

export default config
