import { type Config } from 'tailwindcss'
import defaultTheme from 'tailwindcss/defaultTheme'

export default {
  content: ['./src/**/*.{js,ts,jsx,tsx}'],
  darkMode: 'class',
  plugins: [require('daisyui'), require('@tailwindcss/typography')],
  theme: {
    fontFamily: {
      heading: ['Lobster', 'sans-serif'],
    },
    extend: {
      fontFamily: {
        sans: ['Poppins', ...defaultTheme.fontFamily.sans],
      },
    },
  },
} satisfies Config
