$(document).ready(() => {
    $("#menuButton").on('click', () => {
        $("#sidePanel").panel("open");
    });

    $("#contactButton").on('click', () => {
        window.location = 'contact.html';
        console.log('clicked');
    });

    $("a.mobile").on('click', (e) => {
        e.preventDefault();

        if (e.target.href !== e.target.baseURI) {
            window.location = e.target.href;
        }
        else {
            $("#sidePanel").panel("close");
        }
    });

    $("#gotolinkedin").on('click', () => {
        window.location = 'https://www.linkedin.com/in/geoffroybaumier';
    });

    $('#gotoresumepdf').on('click', () => {
        window.location = 'doc/resume_no_format.pdf';
    });
});