# geoffb.gitlab.io

Project hosted on Gitlab pages: https://geoffb.gitlab.io/

## About

### Portfolio
T3 app with:
- Typescript
- tRPC
- Prisma

Using [Bun](https://bun.sh/) as a package manager and runtime.

### Backoffice (only to edit the data)
Strapi - Headless CMS

Using Yarn ([Bun not yet compatible with Strapi](https://feedback.strapi.io/feature-requests/p/support-for-bun-runtime)).

### Architecture

![project architecture](/portfolio/public/architecture.png)

## Setup

### Prerequisites
- Install asdf (I use brew)
```
brew install asdf
```

- Install asdf plugins
```
# node
asdf plugin add nodejs https://github.com/asdf-vm/asdf-nodejs.git

# tilt
asdf plugin add tilt

# yarn
asdf plugin add yarn

# bun
asdf plugin add bun
```

- Install the needed tools
```
asdf install

# If it fails during the installation of yarn, you may need to insall missing dependencies:
brew install gpg
```

### Database

PostgreSQL: Hosted on [ElephantSQL](https://elephantsql.com).

This project doesn't have a local database, it uses an online database see About/Backoffice.
It is possible to change this configuration adding an .env file in the `backoffice` directory with a custom databse. It could be a local db running in Docker, an online instance...

The Strapi setup will have to be ran and it might be a good idea to re-generate the prisma schema.


### Development environment

To start the development environement run:
```
tilt up
```

To stop it run:
```
tilt down
```